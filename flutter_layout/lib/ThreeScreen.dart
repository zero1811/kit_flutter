import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThreeScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Protiaa',style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                  ),),
                  Icon(Icons.tag_faces,color: Colors.white,),
                ],
              ),
              Column(
                children: <Widget>[
                  Text('dribbble',style: TextStyle(
                    fontSize: 25,
                    color: Colors.white
                  ),),
                  Text('@realvjy', style: TextStyle(
                    fontSize: 10,
                      color: Colors.white
                  ),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('123shots',style: TextStyle(
                        fontSize: 14,
                          color: Colors.white
                      ),),
                      Text('add new', style: TextStyle(
                        fontSize: 14,
                          color: Colors.white
                      ),),
                    ],
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Text('Behance',style: TextStyle(
                    fontSize: 25,
                      color: Colors.white
                  ),),
                  Text('@realvjy', style: TextStyle(
                      fontSize: 10,
                      color: Colors.white
                  ),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('207shots',style: TextStyle(
                          fontSize: 14,
                          color: Colors.white
                      ),),
                      Text('add new', style: TextStyle(
                          fontSize: 14,
                          color: Colors.white
                      ),),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}