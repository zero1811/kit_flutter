import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TwoScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text('Track your \n work and get \n results ', style: TextStyle(
                fontSize: 20,
                color: Colors.lime,
              ),
              textAlign: TextAlign.center,),
              Text('Structure work in a way that\'s \n best for you. Set priorities. Share \n detaila and assign tasks.', style: TextStyle(
                fontSize: 14,
                color: Colors.lime,
              ),
              textAlign: TextAlign.center,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.more_horiz, color: Colors.lime,),
                  Text('Skip', textAlign: TextAlign.right,)
                ],
              )
            ],
          )
        ),
      ),
    );
  }

}