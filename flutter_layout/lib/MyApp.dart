import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_layout/OneScreen.dart';
import 'package:flutter_layout/ThreeScreen.dart';
import 'package:flutter_layout/TwoScreen.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "Flutter Layout",
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: ThreeScreen(),
    );
  }

}
