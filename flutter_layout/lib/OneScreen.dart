import 'package:flutter/material.dart';

class OneScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.yellow,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.arrow_back, color: Colors.red,),
                  Text('Preview ', style: TextStyle(
                    color: Colors.red,

                  ),)
                ],
              ),
              Text('Perfectly \n suited for \n graphic \n design \n & display \n user', style: TextStyle(
                color: Colors.red,
                fontSize: 50,

              ),
              textAlign: TextAlign.start,),
              Text('Swipe to see more',style: TextStyle(
                color: Colors.red,
                fontSize: 14,
              ),
              textAlign: TextAlign.start,),
            ],
          ),
        ),
      ),
    );
  }

}