class NhaXuatBan{
  String _maNhaXuatBan;
  String _tenNhaXuatBan;
  String _diaChi;

//  NhaXuatBan(_maNhaXuatBan,_tenNhaXuatBan,{String diaChi}){
//   _diaChi = diaChi;
//  }

  String get maNhaXuatBan => _maNhaXuatBan;

  set maNhaXuatBan(String value) {
    _maNhaXuatBan = value;
  }

  String get tenNhaXuatBan => _tenNhaXuatBan;

  String get diaChi => _diaChi;

  set diaChi(String value) {
    _diaChi = value;
  }

  set tenNhaXuatBan(String value) {
    _tenNhaXuatBan = value;
  }

  @override
  String toStringNXB() {
    return 'Mã nhà xuất bản: $_maNhaXuatBan \n Tên nhà xuất bản: $_tenNhaXuatBan \n Địa chỉ: $_diaChi \n';
  }
}