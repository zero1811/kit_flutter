import 'Sach.dart';

void main() {
  List<Sach> dsSach = new List();
  taoDanhSach(dsSach);
  sachThapHon50000(dsSach);
  capNhatGia(dsSach);
  timSach(dsSach);

}
taoDanhSach(List<Sach> dsSach){
  dsSach.add(new Sach('S-001', 'Dễ mèn phưu lưu ký', 'Thiếu nhi', 'TG-0043','Tô Hoài', 'XB-0023', 'Kim Đồng', 'Hà Nội', 4, 2007, 49000));
  dsSach.add(new Sach('S-0033', 'Mắt Biếc', 'Tình Cảm', 'TG-0005', 'Nguyễn Nhật Ánh', 'XB-0225', 'NXB Trẻ', 'Hà Nội', 54, 2012, 58000));
  dsSach.add(new Sach('S-0009', 'Tony buổi sáng - Trên đường băng', 'Kỹ năng sống', 'TG-0087', 'Tony', 'XB-0023', 'NXB Trẻ', 'Hà Nội', 4, 2018, 52000));
  dsSach.add(new Sach('S-0386', 'Tuổi Trẻ Đáng Giá Bao Nhiêu', 'Kỹ năng sống', 'TG-0774', 'Đặng Nguyễn Đông Vy', 'XB-0032', 'Nhã Nam', 'Hồ Chí Minh', 18, 2018, 52000));
  dsSach.add(new Sach('S-94538', 'Cuộc sống \'Đếch\' giống cuộc đời', 'Kỹ năng sống', 'TG-4336', 'Hoàng Hải Nguyễn', 'XB-0748', 'Nhà xuất bản Hà Nội', 'Hà Nội', 1, 2020, 63000));
  dsSach.add(new Sach('S0075', 'Tôi là Bêtô','Truyền dài', 'TG-0005',' Nguyễn Nhật Ánh', 'XB-0225', 'NXB Trẻ', 'Hà Nội', 45, 2007, 39000));
  dsSach.add(new Sach('S-0538', 'Cô gái đến tù hôn qua', 'Tuyện dài', 'TG-0005', 'Nguyễn Nhật Ánh', 'XB-0748', 'NXB Trẻ', 'Hà Nội', 39, 2007, 50000));
}
sachThapHon50000(List<Sach> dsSach){
  print("Sách có giá tiền thấp hơn hoặc bằng 50000");
  for(int i = 0,stt = 1 ; i < dsSach.length ; i++){
    if (dsSach.elementAt(i).giaTien < 50000){
      print('$stt : ${dsSach.elementAt(i).tenSach}');
      stt++;
    }
  }
}
capNhatGia(List<Sach> dsSach){
  print("Cập nhật giá \' Dễ mèn phưu lưu ký \' ");
  var tenSachCapNhat = 'Dễ mèn phưu lưu ký';
  for(int i = 0 ; i < dsSach.length ; i++){
    if (dsSach.elementAt(i).tenSach == tenSachCapNhat) {
      dsSach.elementAt(i).giaTien = 225000;
      print(dsSach.elementAt(i).toString());
    }
  }
}
timSach(List<Sach> dsSach){
  print("Sách cần tìm là : ");
  for(int i = 0,stt = 1 ; i < dsSach.length ; i++){
    if (dsSach.elementAt(i).tenSach == "Tony buổi sáng - Trên đường băng" || dsSach.elementAt(i).tenTacGia == "Nguyễn Nhật Ánh" || dsSach.elementAt(i).tenNhaXuatBan == "NXB Trẻ"){
      print(' $stt : ${dsSach.elementAt(i).toString()}');
      stt++;
    }
  }
}