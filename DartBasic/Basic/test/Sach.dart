import 'NhaXuatBan.dart';
import 'TacGia.dart';

class Sach extends TacGia with NhaXuatBan{
  String _maSach;
  String _tenSach;
  String _theLoai;
  int _soLanTaiBan;
  int _namXuatBan;
  int _giaTien;


  Sach(this._maSach, this._tenSach, this._theLoai,_maTacGia,_tenTacGia,_maNhaXuatBan,_tenNhaXuatBan,_diaChi,this._soLanTaiBan,
      this._namXuatBan, this._giaTien) : super(_maTacGia,_tenTacGia){
    super.maNhaXuatBan = _maNhaXuatBan;
    super.tenNhaXuatBan = _tenNhaXuatBan;
    super.diaChi = _diaChi;
  }

  String get maSach => _maSach;

  set maSach(String value) {
    _maSach = value;
  }

  String get tenSach => _tenSach;

  set tenSach(String value) {
    _tenSach = value;
  }

  int get giaTien => _giaTien;

  set giaTien(int value) {
    _giaTien = value;
  }

  int get namXuatBan => _namXuatBan;

  set namXuatBan(int value) {
    _namXuatBan = value;
  }

  int get soLanTaiBan => _soLanTaiBan;

  set soLanTaiBan(int value) {
    _soLanTaiBan = value;
  }

  String get theLoai => _theLoai;

  set theLoai(String value) {
    _theLoai = value;
  }

  @override
  String toString() {
    return 'Mã sách: $_maSach \n Tên sách: $_tenSach\n Thể loại: $_theLoai \n ${super.toStringTG()} ${toStringNXB()} Số lần tái bản: $_soLanTaiBan \n Năm xuất bản: $_namXuatBan \n Giá tiền: $_giaTien';
  }
}