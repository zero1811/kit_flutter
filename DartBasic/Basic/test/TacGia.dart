class TacGia{
  String _maTacGia;
  String _tenTacGia;


  TacGia(this._maTacGia, this._tenTacGia);

  String get maTacGia => _maTacGia;

  set maTacGia(String value) {
    _maTacGia = value;
  }

  String get tenTacGia => _tenTacGia;

  set tenTacGia(String value) {
    _tenTacGia = value;
  }

  @override
  String toStringTG() {
    return 'Mã tác giả: $_maTacGia \n Tên tác giả: $_tenTacGia \n';
  }
}